/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//package com.mycompany.ejb;
import com.mycompany.BookDTO;
import com.mycompany.UserDTO;
import com.mycompany.ejb.BookManagerBeanIfc;
import com.mycompany.ejb.UserManagerBeanIfc;
import com.mycompany.ejb.model.Book;
import com.mycompany.ejb.model.User;
import com.mycompany.UserSession;
import com.mycompany.ejb.model.Author;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author verit
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {

   @EJB
   private BookManagerBeanIfc bookBean;
   @EJB
   private UserManagerBeanIfc userBean;
   BookDTO bookDTO = new BookDTO();
   UserSession currentSession = new UserSession();

    /**
     * Creates a new instance of ApplicationController
     */
    public ApplicationController() {

    }

    public List<BookDTO> getBooks() {
        List<Book> books = bookBean.getBooks();
        List<BookDTO> bookDTOs = new ArrayList<BookDTO>();
        for (int i=0; i < books.size(); i++) {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setTitle(books.get(i).getTitle());
            //bookDTO.setAuthor(books.get(i).getAuthor());
            bookDTO.setIsbn(books.get(i).getIsbn());
            bookDTOs.add(bookDTO);
           
        }
        return bookDTOs;
        
        
    }
    public void registerUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        userBean.registerUser(user);

    }
    
            
    public void saveBook(BookDTO bookDTO){
        Book book = new Book();
        book.setTitle(bookDTO.getTitle());
        book.setIsbn(bookDTO.getIsbn());
        //book.setAuthors(bookDTO.getAuthors());
        String[] authorNames = bookDTO.getAuthors().split(",");
        Collection<Author> bookAuthors = new ArrayList<Author>();
        for (String name : authorNames) {
            Author author = new Author();
            author.setName(name);
            Collection <Book> books = new ArrayList<Book>();
            books.add(book);
            author.setBooks(books);
            bookAuthors.add(author);
        }
        
        book.setAuthors(bookAuthors);
        bookBean.saveBook(book);
    }
}
