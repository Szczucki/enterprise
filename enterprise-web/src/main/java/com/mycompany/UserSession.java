/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.ejb.BookManagerBeanIfc;
import com.mycompany.ejb.UserManagerBeanIfc;
import com.mycompany.ejb.model.User;
import javax.inject.Named;
//import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "userSession")
@SessionScoped

public class UserSession implements Serializable {

    @EJB
    private UserManagerBeanIfc userBean;
    
    private Boolean logged = Boolean.FALSE;
    private Boolean isAdmin = Boolean.FALSE;
    private String userLogin;
    
    public String checkUser(UserDTO userDTO) {

        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        List<User> userList = userBean.getUsers(user);
        if (userList.isEmpty()) {
            this.setLogged(Boolean.FALSE);
            return "/login.xhtml";
        }

        this.setUserLogin(userList.get(0).getLogin());
        this.setIsAdmin(userList.get(0).getIsAdmin());
        this.setLogged(Boolean.TRUE);

        return "/index.xhtml";

    }
    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    
    
    public UserSession() {
    }
    
}
