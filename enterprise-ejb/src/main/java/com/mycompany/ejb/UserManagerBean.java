/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb;

import com.mycompany.ejb.model.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author verit
 */
@Stateless
@LocalBean
public class UserManagerBean implements UserManagerBeanIfc {

    @PersistenceContext(unitName = "persistence-unit")
    private EntityManager em;
    
    @Override
    public List<User> getUsers(User user) {
        //Query query = em.createQuery("FROM User ");
        //return query.getResultList();
        
        String hql = "FROM User u where u.login = :userLogin and u.password = :userPassword";
        List<User> userList = em.createQuery(hql)
                .setParameter("userLogin", user.getLogin())
                .setParameter("userPassword", user.getPassword())
                .getResultList();
        return userList;
    }

    @Override
    public void registerUser(User user) {
        user.setIsAdmin(Boolean.FALSE);
        em.persist(user);    
    }
    
    //public checkUser (User user)

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
