/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb;

import com.mycompany.ejb.model.User;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.List;

/**
 *
 * @author verit
 */
@Local
@Remote
public interface UserManagerBeanIfc {
    
    public List<User> getUsers(User user);
    public void registerUser(User user);
}
