/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb;

import com.mycompany.ejb.model.Book;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author RENT
 */
@Remote
@Local
public interface BookManagerBeanIfc {
    
    public List<Book> getBooks();
    public void saveBook(Book book);
}
    
    
