/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejb;

import com.mycompany.ejb.model.Book;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author RENT
 */
@Stateless
public class BookManagerBean implements BookManagerBeanIfc {
    
   

    @PersistenceContext(unitName = "persistence-unit")
    private EntityManager em;

    @Override
    public List<Book> getBooks() {
        Query query = em.createQuery("FROM Book");
        return query.getResultList();
        
    }

    @Override
    public void saveBook(Book book) {     
        em.persist(book);
    }

    
    
    
}
